var data = { "books": [] };
exports.get = () => {
    return data;
}
exports.add = (book) => {
    data.books.push(book);
}
exports.find = (id) => {
    for (var i = 0  ; i < data.books.length; i++) {
        if (data.books[i].id === parseInt(id)) {
            return data.books[i];
        }
    }
    return {};
}
exports.upd = (book) => {
    var idx = -1;
    for (var i = 0  ; i < data.books.length; i++) {
        if (data.books[i].id === book.id) {
            idx = i;
            break;
        }
    }
    if (idx !== -1) {
        data.books[idx].titulo = book.titulo;
        data.books[idx].autor = book.autor;
    }
}
exports.del = (id) => {
    var idx = -1;
    for (var i = 0  ; i < data.books.length; i++) {
        if (data.books[i].id === parseInt(id)) {
            idx = i;
            break;
        }
    }
    if (idx !== -1) {
        data.books.splice(idx, 1);
    }
}